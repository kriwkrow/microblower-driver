# Microblower Driver

Electronics design for the Murata [MZB1001T02](https://www.mouser.fi/datasheet/2/281/Murata_MZB1001T02_datasheet-1186478.pdf) and/or [MZB3004T04](https://www.mouser.fi/datasheet/2/281/1/Murata_MZB3004T04_Datasheet_201217__1953576-2256433.pdf) microblowers. 

## Dependencies

This project is build using KiCad. Instead of default KiCad libraries, the [Fab](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad) and [Aalto Fablab](https://gitlab.com/aaltofablab/aalto-fablab-kicad-library) parts libraries are used.

## License

(c) Krisjanis Rijnieks 2023
